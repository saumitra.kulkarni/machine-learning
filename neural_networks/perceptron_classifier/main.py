from sklearn import datasets
from sklearn.model_selection import train_test_split
from perceptron import Perceptron

"""
We will use Breast Cancer Data Set
ref.: https://archive.ics.uci.edu/ml/datasets/breast+cancer
Note: sklean breast cancer data is used for training the model in order to 
classify / predict the breast cancer.
"""

# Load the dataset
bc = datasets.load_breast_cancer()

X = bc.data
y = bc.target

# Split the predictor and response into training and testing datasets
X_train, X_test, y_train, y_test = train_test_split(X, y, 
                                test_size=0.3, random_state=42, stratify=y)

# Perceptron classifier
prcptrn = Perceptron()

# Fit the model
prcptrn.fit(X_train, y_train)

# Training score
print("Train Score: ",prcptrn.score(X_train.astype(int), y_train.astype(int)))
# Testing score
print("Test Score: ",prcptrn.score(X_test.astype(int), y_test.astype(int)))
