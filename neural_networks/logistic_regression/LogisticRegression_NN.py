# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""
import numpy as np

class LogisticRegression_NN():
    """
    Logistic regression classifier using neural networks
    """
    def __init__(self, lr = 0.5, itr = 2000, print_cost = False):
        # Number of iterations
        self.itr = itr
        # Learning rate of stochastic gradient descent
        self.lr = lr
        # To print costs
        self.print_cost = print_cost
        # # Weights
        # self.w = None
        # # Bias
        # self.b = None
        
        
    @staticmethod
    def sigmoid(z):
        """
        Compute the sigmoid of z
        
        Parameters
        ----------
        z : ndarray
            Scalar or ndarray
        
        Returns
        -------
        Sigmoid function value.
        
        """
        s = 1/(1 + np.exp(-z))
        
        return s
    
    def propagate(self, X, Y):
        """
        Compute the cost function and its gradient for the propagation
        
        Parameters
        ----------
        w : ndarray
            Vector of weights
        b : float
            Bias
        X : ndarray
            Array of predictor variables
        Y : ndarray
            Array of responce variable
        
        Returns
        -------
        cost : ndarray 
            Array of negative log-likelihood cost for logistic regression
        grads : dict
            Dictionary containing gradient of cost function w.r.t. w and b respectively
            
        """
        
        self.m = X.shape[1]
        
        # Forward Propogation (From X to cost)
        # Compute activation function values
        A = self.sigmoid(np.dot(self.w.T,X) + self.b)
        # Compute cost function values
        cost = np.sum(((- np.log(A))*Y.T + (-np.log(1-A))*(1-Y.T)))/self.m
        
        # Backward Propogation (To find gradients)
        dw = (np.dot(X,(A-Y).T))/self.m
        db = (np.sum(A-Y))/self.m

        assert(dw.shape == self.w.shape)
        assert(db.dtype == float)
        cost = np.squeeze(cost)
        assert(cost.shape == ())
        
        cost = np.squeeze(np.array(cost))

        grads = {'dw': dw, 'db': db}
        
        return grads, cost

    def optimize(self, X, y):
        """
        Optimizes weights and bias using gradient descent algorithm
        
        Parameters
        ----------
        w : ndarray
            Vector of weights
        b : float
            Bias
        X : ndarray
            Array of predictor variables
        Y : ndarray
            Array of responce variable
        itr : int, optional
            Number of iterations, default value is 100
        lr : float, optional
            Learning rate of gradient descent, default value is 0.009
        print_cost : bool, optional
            True for printing the loss at every 100 iterations
        
        Returns
        -------
        params : dict 
            Dictionary containing weights (w) and biases (b)
        grads : dict
            Gradients of cost function w.r.t. w and b respectively.
        Costs : array_like
            All costs computed during the optimization. 
            This can be later used to plot the learning curve.
        
        """
        
        costs = []
        
        for i in range(self.itr):
            # Cost and gradient calculation
            grads, cost = self.propagate(X, y)
            # Retrieve derivatives from grads
            dw = grads["dw"]
            db = grads["db"]
            
            # Update rule
            self.w = self.w - (self.lr * dw)
            self.b = self.b - (self.lr * db)
            
            # Record the costs
            if i % 100 == 0:
                costs.append(cost)
            
            # Print the cost every 100 training iterations
            if self.print_cost and i % 100 == 0:
                print (f'Cost after iteration {i}: {cost}')
        
        params = {'w': self.w, 'b': self.b}
        
        grads = {'dw': dw, 'db': db}
        
        return params, grads, costs

    def predict(self, X):
        """
        Predict response using logistic regression parameters (w, b)
        
        Parameters
        ----------
        w : ndarray
            Vector of weights
        b : float
            Bias
        X : ndarray
            Array of predictor variables
        
        Returns
        -------
        y_predict : ndarray 
            Predictions (0/1) for the given predictor variables
     
        """
        
        m = X.shape[1]
        y_predict = np.zeros((1, m))
        w = self.w.reshape(X.shape[0], 1)
        
        # Compute the probabilities of success
        A = self.sigmoid(np.dot(w.T,X) + self.b)
        
        # Vectorised Implementation
        y_predict = (A >= 0.5) * 1.0
        
        assert(y_predict.shape == (1, m))
        
        return y_predict

    def fit(self, X_train, y_train):
        """
        Build the logistic regression model
        
        Parameters
        ----------
        X_train : ndarray
            Array of training response set
        Y_train : ndarray
            Array of training predictor set
        X_test : ndarray
            Array of test response set
        Y_test : ndarray
            Array of test predictor set
        itr : int, optional
            Number of iterations, default value is 100
        lr : float, optional
            Learning rate of gradient descent, default value is 0.009
        print_cost : bool, optional
            True for printing the loss at every 100 iterations
        
        Returns
        -------
        d : dict 
            Model information like predicted response on train and test set, 
            values of learning rate, number of iterations, cost fucntion weights
            and biases
            
        """

        dim = X_train.shape[0]

        # Initialize parameters with zeros
        self.w = np.zeros((dim,1))
        self.b = 0.0

        assert(self.w.shape == (dim, 1))
        assert(isinstance(self.b, float) or isinstance(self.b, int))

        # Gradient descent optimization
        parameters, grads, costs = self.optimize(X_train, y_train)
        
        # Retrieve parameters w and b
        self.w = parameters['w']
        self.b = parameters['b']
        
        # Predict test/train set examples
        # y_predict_test = self.predict(X_test)
        # y_predict_train = self.predict(X_train)
        
        # d = {'y_predict_test': y_predict_test, 'y_predict_train': y_predict_train, 
        d = {'learning_rate': self.lr, 'iterations': self.itr, 'w': self.w, 'b': self.b, 'costs': costs}
        
        return d


    def score(self, X, y):
        """
        Model score based on comparison of expected and predicted value

        Parameters
        ----------
        X : ndarray
            Array of response variables
        y : ndarray
            Array of predictor variable
        
        Returns
        -------
        score : float 
            Model score in percentage

        """ 
        score = 100 - np.mean(np.abs(self.predict(X) - y)) * 100
        return score
        
        
     
            
        
